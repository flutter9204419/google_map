import 'package:flutter/material.dart';

class CommonFloatingButton extends StatelessWidget {
  final Function() onPressed;
  final IconData icon;
  final String tooltip;
  const CommonFloatingButton(
      {super.key,
      required this.onPressed,
      required this.icon,
      required this.tooltip});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPressed,
      tooltip: tooltip,
      child: Icon(icon),
    );
  }
}
