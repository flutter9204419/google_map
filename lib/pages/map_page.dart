import 'package:flutter/material.dart';
import 'package:google_map/controller/map_page_controller.dart';
import 'package:google_map/widgets/common_floating_button.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends MapPageController {
  @override
  void initState() {
    super.initState();
    addCustomIcon();
    addNetworkIcon();
    getLocationUpdate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: GoogleMap(
        onMapCreated: ((GoogleMapController controller) =>
            mapController.complete(controller)),
        initialCameraPosition: CameraPosition(
          target: currentP ?? pIsconLocation,
          zoom: 13,
        ),
        markers: markers,
        polylines: Set<Polyline>.of(polylines.values),
        trafficEnabled: showTraffic,
        mapType: mapType[selectedValue],
        onTap: (LatLng marker) {
          markers.add(
            Marker(
              markerId: MarkerId('_newmarke${markers.length}'),
              position: marker,
              icon: BitmapDescriptor.defaultMarkerWithHue(
                  BitmapDescriptor.hueViolet),
            ),
          );
          polygonPoint.add(marker);

          // Check if there are more than four points and add the polygon
          if (polygonPoint.length > 3) {
            polygons.clear();
            polygons.add(
              Polygon(
                polygonId: const PolygonId("1"),
                points: polygonPoint,
                fillColor: const Color(0xff006491).withOpacity(0.2),
                strokeWidth: 2,
              ),
            );
          }
          setState(() {});
        },

        //set circle for geo fencing
        circles: {
          Circle(
            circleId: const CircleId("1"),
            center: currentP ?? pIsconLocation,
            radius: geofenceRadius,
            strokeWidth: 1,
            fillColor: const Color(0xff006491).withOpacity(0.2),
          ),
        },

        //set Polygon area for geo fencing
        polygons: polygons,
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(height: 80),
          // Add Static marker
          CommonFloatingButton(
            onPressed: () {
              // Add a custom marker
              markers.addAll({
                Marker(
                  markerId: const MarkerId("_currentLocation"),

                  infoWindow: InfoWindow(
                    title: "Current Location",
                    snippet: "Your current location",
                    onTap: () {},
                    //set infowindow position
                    // anchor: const Offset(1, -1),
                  ),

                  //default marker with red color
                  // icon: BitmapDescriptor.defaultMarker,

                  //chnage color value between 0 to 359 only
                  // icon: BitmapDescriptor.defaultMarkerWithHue(200),

                  //custom marker
                  icon: markerIcon,
                  position: currentP ?? pIsconLocation,
                ),
                Marker(
                  markerId: const MarkerId("_sourceLocation"),
                  icon: BitmapDescriptor.defaultMarkerWithHue(200),
                  position: pIsconLocation,
                ),
                Marker(
                  markerId: const MarkerId("_destinationLocation"),
                  // icon: BitmapDescriptor.defaultMarker,
                  icon: networkIcon,
                  position: pKalupur,
                ),
              });
              setState(() {});
            },
            icon: Icons.add_location,
            tooltip: "Add Specific location",
          ),
          const SizedBox(height: 10),

          // Generate Polyline from given location
          CommonFloatingButton(
              onPressed: () {
                // Draw a route
                getPolyLinePoints().then(
                  (coordinates) => {
                    generatePolylineFromPoints(coordinates),
                  },
                );

                setState(() {});
              },
              icon: Icons.directions,
              tooltip: "Make route"),
          const SizedBox(height: 10),

          // Indicate traffic
          CommonFloatingButton(
              onPressed: () {
                // Toggle traffic layer
                setState(() {
                  showTraffic = !showTraffic;
                });
              },
              icon: Icons.traffic,
              tooltip: "Show Traffic"),
          const SizedBox(height: 10),

          // Change Map Type
          CommonFloatingButton(
              onPressed: () {
                // Change map type

                showRadioDialog(context);
              },
              icon: Icons.map,
              tooltip: "Change Map Mode"),
          const SizedBox(height: 10),

          // Focus Current Position
          CommonFloatingButton(
              onPressed: () {
                cameraToPostion(currentP ?? pIsconLocation);
              },
              icon: Icons.location_searching,
              tooltip: "Camera Position"),
          const SizedBox(height: 10),

          // Clear All marker from the map
          CommonFloatingButton(
              onPressed: () {
                markers.clear();
                polygonPoint.clear();
                polygons.clear();
                setState(() {});
              },
              icon: Icons.wrong_location_outlined,
              tooltip: "Remove Marker"),
          const SizedBox(height: 10),

          // Gio Fencing distance find formula
          CommonFloatingButton(
              onPressed: findDistance,
              icon: Icons.social_distance,
              tooltip: "Find Distance"),
        ],
      ),
    );
  }
}
