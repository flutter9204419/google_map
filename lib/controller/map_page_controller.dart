import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_map/consts.dart';
import 'package:google_map/pages/map_page.dart';
import 'package:google_map/utils/utility.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

abstract class MapPageController extends State<MapPage> {
  // Current location controller
  final Location locationController = Location();

  // static location coordinates
  LatLng pGooglePlex = const LatLng(37.4223, -122.0848);
  LatLng pIsconLocation = const LatLng(23.024619449897653, 72.50702841135082);
  LatLng pApplePark = const LatLng(37.3346, -122.0090);
  LatLng pKalupur = const LatLng(23.028683008507958, 72.60035922669417);

  //current location variable
  LatLng? currentP;

  // map controller for draw map
  final Completer<GoogleMapController> mapController =
      Completer<GoogleMapController>();

  // points for draw polylines
  Map<PolylineId, Polyline> polylines = {};

  // marker variable
  Set<Marker> markers = {};

  // traffic indicate variable
  bool showTraffic = false;

  //mapType changes variables
  List<MapType> mapType = [
    MapType.terrain,
    MapType.hybrid,
    MapType.satellite,
    MapType.normal,
    MapType.none,
  ];
  int selectedValue = 1;

  //gio fencing variables
  LatLng geofenceCenter = const LatLng(37.7749, -122.4194);
  double geofenceRadius = 4000.0;

  // Custom Marker var
  BitmapDescriptor markerIcon = BitmapDescriptor.defaultMarker;
  BitmapDescriptor networkIcon = BitmapDescriptor.defaultMarker;

  // polygons setup variable
  List<LatLng> polygonPoint = [];
  Set<Polygon> polygons = {};

  // Assets image marker
  void addCustomIcon() {
    BitmapDescriptor.fromAssetImage(
            const ImageConfiguration(), "assets/images/pin1.png")
        .then(
      (icon) {
        setState(() {
          markerIcon = icon;
        });
      },
    );
  }

  //network image marker
  void addNetworkIcon() async {
    var url =
        "https://imgs.search.brave.com/ToRL11Zaeri9UOAx-444I8nJUmI4iaN2YB7KFwbHRFs/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWcu/dG9vbHN0dWQuaW8v/MjQweDI0MC8zYjU5/OTgvZmZmJnRleHQ9/KzY0eDY0Kw";
    var bytes = (await NetworkAssetBundle(Uri.parse(url)).load(url))
        .buffer
        .asUint8List();
    setState(() {
      networkIcon = BitmapDescriptor.fromBytes(bytes);
    });
  }

  // find distance between two coordinates (use for gio fencing)
  void findDistance() {
    var distanceBetween = Utility.haversineDistance(
        LatLng(currentP?.latitude ?? pKalupur.latitude,
            currentP?.longitude ?? pKalupur.longitude),
        LatLng(pIsconLocation.latitude, pIsconLocation.longitude));
    debugPrint('distance between... $distanceBetween');
    if (distanceBetween > geofenceRadius) {
      /// I simply show a snackBar message here you can implement your custom logic here.
      Utility.showSankBar(context,
          msg: 'You are outside the area', bgColor: Colors.red);
    } else {
      Utility.showSankBar(context,
          msg: 'You are inside the area', bgColor: Colors.green);
    }
  }

  // method for update user live location
  Future<void> getLocationUpdate() async {
    bool serviceEnabled;
    PermissionStatus permissionGranted;

    serviceEnabled = await locationController.serviceEnabled();
    if (serviceEnabled) {
      serviceEnabled = await locationController.requestService();
    } else {
      return;
    }

    permissionGranted = await locationController.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await locationController.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    //listen in every second for user live location
    locationController.onLocationChanged.listen((LocationData currentLocation) {
      if (currentLocation.latitude != null &&
          currentLocation.longitude != null) {
        setState(() {
          currentP =
              LatLng(currentLocation.latitude!, currentLocation.longitude!);

          // Focus on Current postion
          // _cameraToPostion(_currentP!);
        });
      }
    });
  }

  //for getting every latLag point between source and destination location
  Future<List<LatLng>> getPolyLinePoints() async {
    List<LatLng> polylineCoordinates = [];
    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      kGoogleMapsAPI,
      PointLatLng(pIsconLocation.latitude, pIsconLocation.longitude),
      PointLatLng(pKalupur.latitude, pKalupur.longitude),
      travelMode: TravelMode.driving,
    );
    if (result.points.isNotEmpty) {
      for (var point in result.points) {
        polylineCoordinates.add(
          LatLng(point.latitude, point.longitude),
        );
      }
    } else {
      debugPrint(result.errorMessage.toString());
    }
    return polylineCoordinates;
  }

  // draw polyline points between source and destination location
  void generatePolylineFromPoints(List<LatLng> polylineCoordinates) async {
    PolylineId id = const PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.blue,
      points: polylineCoordinates,
      width: 8,
    );
    setState(() {
      polylines[id] = polyline;
    });
  }

  // re center camera position
  Future<void> cameraToPostion(LatLng pos) async {
    final GoogleMapController controller = await mapController.future;
    CameraPosition newCameraPosition = CameraPosition(
      target: pos,
      zoom: 15,
    );
    await controller
        .animateCamera(CameraUpdate.newCameraPosition(newCameraPosition));
  }

  // radio button dialog box
  void showRadioDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Select Map Type'),
          content: StatefulBuilder(
            builder: (context, setState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  RadioListTile(
                    title: const Text('Terrain View'),
                    value: 0,
                    groupValue: selectedValue,
                    onChanged: (value) {
                      setState(() {
                        selectedValue = value!;
                      });
                    },
                  ),
                  RadioListTile(
                    title: const Text('Hybrid View'),
                    value: 1,
                    groupValue: selectedValue,
                    onChanged: (value) {
                      setState(() {
                        selectedValue = value!;
                      });
                    },
                  ),
                  RadioListTile(
                    title: const Text('Satellite View'),
                    value: 2,
                    groupValue: selectedValue,
                    onChanged: (value) {
                      setState(() {
                        selectedValue = value!;
                      });
                    },
                  ),
                  RadioListTile(
                    title: const Text('Normal View'),
                    value: 3,
                    groupValue: selectedValue,
                    onChanged: (value) {
                      setState(() {
                        selectedValue = value!;
                      });
                    },
                  ),
                  RadioListTile(
                    title: const Text('None'),
                    value: 4,
                    groupValue: selectedValue,
                    onChanged: (value) {
                      setState(() {
                        selectedValue = value!;
                      });
                    },
                  ),
                ],
              );
            },
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                setState(() {});
                Navigator.of(context).pop(
                    selectedValue); // Close the dialog and return the selected value
              },
              child: const Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
